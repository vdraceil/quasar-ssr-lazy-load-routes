
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '',
        component: () => import('pages/Index.vue'),
        children: [
          {
            path: 'type1',
            component: () => import(
              /* webpackChunkName: "type1" */
              'pages/type1/Index.vue'
            ),
            children: [{
              path: 'a', component: () => import(
                /* webpackChunkName: "type1" */
                'pages/type1/Page1A.vue'
              )
            }, {
              path: 'b', component: () => import(
                /* webpackChunkName: "type1" */
                'pages/type1/Page1B.vue'
              )
            }, {
              path: 'c', component: () => import(
                /* webpackChunkName: "type1" */
                'pages/type1/Page1C.vue'
              )
            }]
          },
          {
            path: 'type2',
            component: () => import(
              /* webpackChunkName: "type2" */
              'pages/type2/Index.vue'
            ),
            children: [{
              path: 'a', component: () => import(
                /* webpackChunkName: "type2" */
                'pages/type2/Page2A.vue'
              )
            }, {
              path: 'b', component: () => import(
                /* webpackChunkName: "type2" */
                'pages/type2/Page2B.vue'
              )
            }, {
              path: 'c', component: () => import(
                /* webpackChunkName: "type2" */
                'pages/type2/Page2C.vue'
              )
            }]
          },
          {
            path: 'type3',
            component: () => import(
              /* webpackChunkName: "type3" */
              'pages/type3/Index.vue'
            ),
            children: [{
              path: 'a', component: () => import(
                /* webpackChunkName: "type3" */
                'pages/type3/Page3A.vue'
              )
            }, {
              path: 'b', component: () => import(
                /* webpackChunkName: "type3" */
                'pages/type3/Page3B.vue'
              )
            }, {
              path: 'c', component: () => import(
                /* webpackChunkName: "type3" */
                'pages/type3/Page3C.vue'
              )
            }]
          }
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
